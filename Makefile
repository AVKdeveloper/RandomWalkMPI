COMPILER = mpicc
CFLAGS = -Wall -Werror -std=c99

run: main.o list.o vector.o migration.o
	$(COMPILER) $(CFLAGS) main.o list.o vector.o migration.o -o run
main.o: main.c
	$(COMPILER) $(CFLAGS) main.c -c
test: unit_tests.o list.o vector.o migration.o
	$(COMPILER) $(CFLAGS) unit_tests.o list.o vector.o migration.o -o test
unit_tests.o: unit_tests.c
	$(COMPILER) $(CFLAGS) unit_tests.c -c
list.o: list.c list.h
	$(COMPILER) $(CFLAGS) list.c -c
vector.o: vector.c vector.h
	$(COMPILER) $(CFLAGS) vector.c -c
migration.o: migration.c migration.h
	$(COMPILER) $(CFLAGS) migration.c -c
clean:
	rm *.o run test core.* slurm-* stats.txt
