#include <assert.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "list.h"
#include "migration.h"
#include "vector.h"

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (argc != 10) {
        fprintf(stderr, "Wrong number of arguments!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    int l = atoi(argv[1]);
    if (l < 1) {
        fprintf(stderr, "Wrong length of square side!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    int a = atoi(argv[2]);
    int b = atoi(argv[3]);
    if (a < 1 || b < 1) {
        fprintf(stderr, "Wrong number of squares!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    if (a * b != size) {
        fprintf(stderr, "Number of squares isn't equal to number of nodes!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    int n = atoi(argv[4]);
    if (n < 1) {
        fprintf(stderr, "Wrong number of jumps for particles!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    int N = atoi(argv[5]);
    if (N < 1) {
        fprintf(stderr, "Wrong number of particles in each square!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    double p_l = atof(argv[6]);
    double p_r = atof(argv[7]);
    double p_u = atof(argv[8]);
    double p_d = atof(argv[9]);
    if (fabs(p_l + p_r + p_u + p_d - 1) > 1E-10) {
        fprintf(stderr, "Wrong values of probabilities of jumps!\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // Starting time
    double start_time = MPI_Wtime();
    srand(time(NULL) + rank * 100);
    // Create new MPI datatype for point_t serialization
    MPI_Datatype type_point;
    int array_of_blocklengths[4] = {1, 1, 1, 1};
    MPI_Aint array_of_displacements[4];
    array_of_displacements[0] = offsetof(point_t, x);
    array_of_displacements[1] = offsetof(point_t, y);
    array_of_displacements[2] = offsetof(point_t, iterations_remain);
    array_of_displacements[3] = offsetof(point_t, source);
    MPI_Datatype array_of_types[4] = {MPI_INT, MPI_INT, MPI_UNSIGNED,
            MPI_UNSIGNED};
    MPI_Type_create_struct(4, array_of_blocklengths, array_of_displacements,
            array_of_types, &type_point);
	MPI_Type_commit(&type_point);
    // Initializing variable for statistics
    int dead_particles = 0;
    // Initializing list with new particles
    list_t *list = list_create(size * N);
    for (int i = 0; i < N; ++i) {
        point_t *point = point_create(rank, n, l, a, b);
        list_add(list, point);
    }
    // Initializing vectors for sending particles
    vector_t **vectors = (vector_t **)malloc(size * sizeof(vector_t *));
    for (int i = 0; i < size; ++i) {
        vectors[i] = vector_create(0, sizeof(point_t));
    }
    // Do migration of particles
    for (int k = 0; ; k++) {
        list_node_t *previous_node = NULL;
        list_node_t *current_node = NULL;
        while ((current_node = list_get_next_node(list, previous_node))
                != NULL) {
            point_t *point = (point_t *)current_node->data;
            assert(point_get_square(point, l, a, b) == rank);
            if (point->iterations_remain <= 0) {
                dead_particles++;
                list_remove(list, previous_node);
                point_destroy(point);
            } else {
                point_jump(point, p_l, p_r, p_d, p_u, l, a, b);
                int point_location = point_get_square(point, l, a, b);
                if (point_location != rank) {
                    // Will send this point to another processor
                    list_remove(list, previous_node);
                    vector_put(vectors[point_location], point);
                    point_destroy(point);
                } else {
                    previous_node = current_node;
                }
            }
        }
        // Get statistic about current quantity of living particles
        int all_particles = 0;
        MPI_Allreduce((void *)&dead_particles, (void *)&all_particles, 1,
                MPI_INT, MPI_SUM, MPI_COMM_WORLD);
        if (all_particles == N * size) {
            // Break criterion is that all particles are dead
            assert(list_size(list) == 0);
            // Another criterion is n iterations in synchronous case
            assert(k == n);
            break;
        }
        // Exchange of gone particles
        for (int i = 0; i < rank; ++i) {
            MPI_Status status;
            MPI_Probe(i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            int count;
            MPI_Get_count(&status, type_point, &count);
            vector_resize(vectors[rank], count);
            MPI_Recv(vectors[rank]->data, count, type_point, i, MPI_ANY_TAG,
                    MPI_COMM_WORLD, &status);
            vectors[rank]->size = count;
            while (vectors[rank]->size > 0) {
                point_t *point = point_copy((point_t *)
                        vector_remove(vectors[rank]));
                list_add(list, (void *)point);
            }
            vector_clear(vectors[rank]);
        }
        for (int i = rank + 1; i < size; ++i) {
            MPI_Send(vectors[i]->data, vectors[i]->size, type_point, i,
                    0, MPI_COMM_WORLD);
            vector_clear(vectors[i]);
        }
        for (int i = rank + 1; i < size; ++i) {
            MPI_Status status;
            MPI_Probe(i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            int count;
            MPI_Get_count(&status, type_point, &count);
            vector_resize(vectors[rank], count);
            MPI_Recv(vectors[rank]->data, count, type_point, i, MPI_ANY_TAG,
                    MPI_COMM_WORLD, &status);
            vectors[rank]->size = count;
            while (vectors[rank]->size > 0) {
                point_t *point = point_copy((point_t *)
                        vector_remove(vectors[rank]));
                list_add(list, (void *)point);
            }
            vector_clear(vectors[rank]);
        }
        for (int i = 0; i < rank; ++i) {
            MPI_Send(vectors[i]->data, vectors[i]->size, type_point, i,
                    0, MPI_COMM_WORLD);
            vector_clear(vectors[i]);
        }
    }

    // Collect output statistics
    if (rank == 0) {
        int *particles_from = (int *)malloc(size * sizeof(int));
        for (int i = 0; i < size; ++i) {
            particles_from[i] = 0;
        }
        MPI_Gather(&dead_particles, 1, MPI_INT,
                (void *)particles_from, 1, MPI_INT, 0, MPI_COMM_WORLD);
        double end_time = MPI_Wtime();
        FILE* file = fopen("stats.txt", "w");
        fprintf(file, "%d %d %d %d %d %f %f %f %f %fs\n", l, a, b, n, N, p_l,
                p_r, p_u, p_d, end_time - start_time);
        for (int i = 0; i < size; ++i) {
            fprintf(file, "%d: %d\n", i, particles_from[i]);
        }
        fclose(file);
        free(particles_from);
    } else {
        MPI_Gather(&dead_particles, 1, MPI_INT, NULL, 1, MPI_INT,
                0, MPI_COMM_WORLD);
    }

    for (int i = 0; i < size; ++i) {
        vector_destroy(vectors[i]);
    }
    free(vectors);
    list_destroy(list);
    MPI_Finalize();
    return 0;
}
