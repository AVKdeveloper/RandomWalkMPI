#include "list.h"

#include <assert.h>

list_node_t *list_node_create(void *data) {
    list_node_t *node = (list_node_t *)malloc(sizeof(list_node_t));
    node->data = data;
    node->next = NULL;
    return data;
}

void list_node_destroy(list_node_t *node) {
    free(node);
}

list_t *list_create(size_t capacity) {
    list_t *list = (list_t *)malloc(sizeof(list_t));
    list->capacity = capacity;
    list->size = 0;
    list->head = NULL;
    list->all_nodes = (list_node_t *)malloc(capacity * sizeof(list_node_t));
    list->head_free = &list->all_nodes[0];
    for (int i = 0; i < capacity - 1; ++i) {
        list->all_nodes[i].next = &list->all_nodes[i + 1];
    }
    return list;
}

void list_destroy(list_t *list) {
    free(list->all_nodes);
    free(list);
}

size_t list_size(const list_t *list) {
    return list->size;
}

void list_add(list_t *list, void *data) {
    assert(list->size < list->capacity);
    list->size++;
    list_node_t *new_node = list->head_free;
    list->head_free = list->head_free->next;
    new_node->data = data;
    new_node->next = list->head;
    list->head = new_node;
}

list_node_t *list_get_next_node(list_t *list, list_node_t *previous) {
    if (previous == NULL) {
        return list->head;
    }
    if (previous->next == NULL) {
        return NULL;
    }
    return previous->next;
}

void *list_remove(list_t *list, list_node_t *previous) {
    assert(list->size > 0);
    list->size--;
    list_node_t *deleting_node;
    if (previous == NULL) {
        deleting_node = list->head;
        list->head = deleting_node->next;
    } else {
        deleting_node = previous->next;
        assert(deleting_node != NULL);
        previous->next = deleting_node->next;
    }
    deleting_node->next = list->head_free;
    list->head_free = deleting_node;
    void *data = deleting_node->data;
    deleting_node->data = NULL;
    return data;
}
