#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "list.h"
#include "vector.h"
#include "migration.h"

void test1();
void test2();
void test3();

int main() {
    printf("Starting tests\n");
    test1();
    test2();
    test3();
    printf("All tests passed successfully!\n");
    return 0;
}

void test1() {
    printf("Test for list_t\n");
    list_t *list = list_create(10);
    list_node_t *node;
    int element1 = 1;
    int element2 = 4;
    int element3 = 2;
    assert(list_size(list) == 0);
    assert(list_get_next_node(list, NULL) == NULL);
    list_add(list, &element1);
    assert(list_size(list) == 1);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 1);
    list_add(list, &element2);
    assert(list_size(list) == 2);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 4);
    node = list_get_next_node(list, node);
    assert(*(int *)(node->data) == 1);
    node = list_get_next_node(list, node);
    assert(node == NULL);
    list_add(list, &element3);
    assert(list_size(list) == 3);
    assert(*(int *)list_remove(list, NULL) == 2);
    assert(list_size(list) == 2);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 4);
    assert(*(int *)list_remove(list, node) == 1);
    assert(list_size(list) == 1);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 4);
    list_add(list, &element1);
    assert(list_size(list) == 2);
    node = list_get_next_node(list, NULL);
    assert(*(int *)(node->data) == 1);
    node = list_get_next_node(list, node);
    assert(*(int *)(node->data) == 4);
    assert(node->next == NULL);
    list_destroy(list);
    printf("OK\n");
}

void test2() {
    printf("Test for vector_t\n");
    vector_t *vector = vector_create(0, sizeof(int));
    int element1 = 1;
    int element2 = 2;
    int element3 = 3;
    int element4 = 4;
    int element5 = 5;
    int element6 = 6;
    int element7 = 7;
    int element8 = 8;
    vector_put(vector, &element1);
    assert(vector->size == 1);
    assert(vector->capacity == 1);
    vector_put(vector, &element2);
    assert(vector->size == 2);
    assert(vector->capacity == 3);
    vector_put(vector, &element3);
    assert(vector->size == 3);
    assert(vector->capacity == 3);
    vector_put(vector, &element4);
    assert(vector->size == 4);
    assert(vector->capacity == 7);
    vector_put(vector, &element5);
    vector_put(vector, &element6);
    vector_put(vector, &element7);
    assert(vector->size == 7);
    assert(vector->capacity == 7);
    for (int i = 0; i < 7; ++i) {
        assert(*(int *)vector_get(vector, i) == i + 1);
    }
    assert(vector->size == 7);
    assert(*(int *)vector_remove(vector) == 7);
    assert(vector->size == 6);
    assert(vector->capacity == 7);
    vector_put(vector, &element8);
    assert(vector->size == 7);
    assert(vector->capacity == 7);
    assert(*(int *)vector_remove(vector) == 8);
    for (int i = 5; i >= 0; --i) {
        assert(*(int *)vector_remove(vector) == i + 1);
        assert(vector->size == i);
    }
    assert(vector_remove(vector) == NULL);
    vector_put(vector, &element5);
    vector_put(vector, &element6);
    vector_put(vector, &element7);
    assert(vector->size == 3);
    assert(vector->capacity == 7);
    vector_clear(vector);
    assert(vector->size == 0);
    assert(vector->capacity == 0);
    vector_put(vector, &element2);
    assert(vector->size == 1);
    assert(vector->capacity == 1);
    vector_destroy(vector);
    printf("OK\n");
}

void test3() {
    printf("Test for point_t\n");
    for (int i = 0; i < 100; ++i) {
        point_t *point = point_create(i, 100, 10, 25, 4);
        assert(point_get_square(point, 10, 25, 4) == i);
        point_destroy(point);
    }
    int iter = 100000;
    int width = 3;
    int height = 2;
    int length = 10;
    point_t *point = point_create(5, iter, length, height, width);
    assert(point_get_square(point, length, height, width) == 5);
    double p_l = 0.3;
    double p_r = 0.2;
    double p_d = 0.4;
    double p_u = 0.1;
    int left_jumps = 0;
    int right_jumps = 0;
    int down_jumps = 0;
    int up_jumps = 0;
    for (int i = 0; i < iter; ++i) {
        int init_x = point->x;
        int init_y = point->y;
        point_jump(point, p_l, p_r, p_d, p_u, length, height, width);
        assert(point->iterations_remain == iter - i - 1);
        if ((point->x - init_x == 1) ||
                (point->x + width * length - init_x == 1)) {
            right_jumps++;
        } else if ((init_x - point->x == 1) ||
                (init_x + width * length - point->x == 1)) {
            left_jumps++;
        } else if ((point->y - init_y == 1) ||
                (point->y + height * length - init_y == 1)) {
            down_jumps++;
        } else if ((init_y - point->y == 1) ||
                (init_y + height * length - point->y == 1)) {
            up_jumps++;
        } else {
            // printf("x=%d y=%d ==> x=%d y=%d\n", init_x, init_y, point->x, point->y);
            assert(0 == 1);
        }
    }
    int total_jumps = right_jumps + left_jumps + up_jumps + down_jumps;
    printf("Total jumps %d\n", total_jumps);
    assert(total_jumps == iter);
    double epsilon = 0.01;
    // printf("Left: %f vs exp: %f\n", p_l, (double)left_jumps / total_jumps);
    // printf("Right: %f vs exp: %f\n", p_r, (double)right_jumps / total_jumps);
    // printf("Down: %f vs exp: %f\n", p_d, (double)down_jumps / total_jumps);
    // printf("Up: %f vs exp: %f\n", p_u, (double)up_jumps / total_jumps);
    assert(fabs((double)right_jumps / total_jumps - p_r) < epsilon);
    assert(fabs((double)left_jumps / total_jumps - p_l) < epsilon);
    assert(fabs((double)down_jumps / total_jumps - p_d) < epsilon);
    assert(fabs((double)up_jumps / total_jumps - p_u) < epsilon);
    point_destroy(point);
    printf("OK\n");
}
