#include "migration.h"

#include <assert.h>
#include <stdio.h>

point_t *point_create(size_t source, size_t iterations_remain,
        size_t square_size, size_t a, size_t b) {
    point_t *point = (point_t *)malloc(sizeof(point_t));
    point->iterations_remain = iterations_remain;
    point->source = source;
    point->x = source % b * square_size + rand() % square_size;
    point->y = source / b * square_size + rand() % square_size;
    return point;
}

point_t *point_copy(point_t *other) {
    point_t *point = (point_t *)malloc(sizeof(point_t));
    point->iterations_remain = other->iterations_remain;
    point->source = other->source;
    point->x = other->x;
    point->y = other->y;
    return point;
}

void point_destroy(point_t *point) {
    free(point);
}

size_t point_get_square(point_t *point, size_t square_size, size_t a,
        size_t b) {
    return point->y / square_size * b + point->x / square_size;
}

void point_jump(point_t *point, double p_left, double p_right, double p_down,
        double p_up, size_t square_size, size_t a, size_t b) {
    assert(point->iterations_remain > 0);
    point->iterations_remain--;
    double chance = (double)rand() / RAND_MAX;
    if (chance < p_left) {
        point->x--;
        if (point->x < 0) {
            point->x = b * square_size - 1;
        }
    } else if (chance < p_left + p_right) {
        point->x++;
        if (point->x > b * square_size - 1) {
            point->x = 0;
        }
    } else if (chance < p_left + p_right + p_down) {
        point->y++;
        if (point->y > a * square_size - 1) {
            point->y = 0;
        }
    } else {
        point->y--;
        if (point->y < 0) {
            point->y = a * square_size - 1;
        }
    }
}
