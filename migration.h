#ifndef RANDOM_WALK_MPI_MIGRATION_H_
#define RANDOM_WALK_MPI_MIGRATION_H_

#include <stdlib.h>

typedef struct point_t {
    int x;
    int y;
    unsigned iterations_remain;
    unsigned source;
} point_t;

point_t *point_create(size_t source, size_t iterations_remain,
        size_t square_size, size_t a, size_t b);
point_t *point_copy(point_t *other);
void point_destroy(point_t *point);
size_t point_get_square(point_t *point, size_t square_size, size_t a, size_t b);
void point_jump(point_t *point, double p_left, double p_right, double p_down,
        double p_up, size_t square_size, size_t a, size_t b);

#endif  // RANDOM_WALK_MPI_MIGRATION_H_
