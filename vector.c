#include "vector.h"

#include <assert.h>
#include <string.h>

vector_t *vector_create(size_t capacity, size_t element_size) {
    vector_t *vector = (vector_t *)malloc(sizeof(vector_t));
    vector->size = 0;
    vector->capacity = capacity;
    vector->element_size = element_size;
    vector->data = calloc(capacity, element_size);
    return vector;
}

void vector_destroy(vector_t *vector) {
    free(vector->data);
    free(vector);
}

void vector_resize(vector_t *vector, size_t new_capacity) {
    vector->data = realloc(vector->data, new_capacity * vector->element_size);
    vector->capacity = new_capacity;
}

void vector_put(vector_t *vector, void *element) {
    if (vector->size >= vector->capacity) {
        vector_resize(vector, vector->capacity * 2 + 1);
    }
    memcpy((void *)((char *)vector->data + vector->size * vector->element_size),
            element, vector->element_size);
    vector->size++;
}

void *vector_remove(vector_t *vector) {
    if (vector->size == 0) {
        return NULL;
    }
    vector->size--;
    return (void *)((char *)vector->data + vector->size * vector->element_size);
}

void *vector_get(vector_t *vector, size_t index) {
    assert(index < vector->size);
    return (void *)((char *)vector->data + index * vector->element_size);
}

void vector_clear(vector_t *vector) {
    vector_resize(vector, 0);
    vector->size = 0;
}
