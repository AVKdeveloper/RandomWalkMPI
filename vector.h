#ifndef RANDOM_WALK_MPI_VECTOR_H_
#define RANDOM_WALK_MPI_VECTOR_H_

#include <stdlib.h>

typedef struct vector_t {
    size_t size;
    size_t capacity;
    size_t element_size;
    void *data;
} vector_t;

vector_t *vector_create(size_t capacity, size_t element_size);
void vector_destroy(vector_t *vector);
void vector_resize(vector_t *vector, size_t new_capacity);
void vector_put(vector_t *vector, void *element);
void *vector_remove(vector_t *vector);
void *vector_get(vector_t *vector, size_t index);
void vector_clear(vector_t *vector);

#endif  // RANDOM_WALK_MPI_VECTOR_H_
